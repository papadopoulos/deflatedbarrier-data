# README #

This repository stores the necessary data files for the convergence plots of 

"Numerical analysis of a topology optimization problemfor Stokes flow", I.P.A. Papadopoulos, E. Suli, 2021. 

This is only a sumplemental repository for the DeflatedBarrier code

(Bitbucket) https://bitbucket.org/papadopoulos/deflatedbarrier/
(GitHub)    https://github.com/ioannisPApapadopoulos/deflatedbarrier

